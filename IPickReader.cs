using System.Collections;

namespace PickData
{
    public interface IPickReader {
        PickId.List PickIdList { get; }
        IProject ConfigurationData { get; }
        IEnumerable IdRenumberList { get; }

        string GetNopData(string id);
        void ReadFromDB();
//        void LoadConfiguration(string configurationFilePath);
    }
}

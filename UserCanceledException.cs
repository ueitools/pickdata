using System;

namespace PickData
{
    public class UserCanceledException : Exception
    {
        public UserCanceledException() : base("User canceled process.") {}
    }
}

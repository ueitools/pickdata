using System;
using System.Collections.Generic;

namespace PickData
{
    public class PickEngineDevice
    {
        private Device _device;
        private PickId.List _pickIds;
        private List _deviceList;

        public PickEngineDevice(Device device)
        {
            _device = device;
            _deviceList = new List(device.DeviceList);
        }

        public Device Device
        {
            get { return _device; }
        }

        public List DeviceList
        {
            get { return _deviceList; }
        }

        public PickId.List PickIds
        {
            get {
                if (_pickIds == null)
                    _pickIds = new PickId.List();
                
                return _pickIds;
            }
        }

        public List<char> SourceDevice
        {
            get { return _device.SourceDevice; }
        }

        public bool Move
        {
            get { return _device.Move; }
        }

        public string Mode
        {
            get { return _device.Mode; }
        }

        public bool DoesMatch(PickId pickID)
        {
            return _device.DoesMatch(pickID);
        }

        public void AddPickId(PickId pickID)
        {
            PickIds.Add(pickID);
        }

        public override string ToString()
        {
            return Device.ToString();
        }

        public class List : List<PickEngineDevice> {
            public List() { }
            public List(Device.List deviceList)
            {
                foreach (Device device in deviceList)
                {
                    Add(new PickEngineDevice(device));
                }
            }

            public char[] GetDeviceLettersInOrderByModes(List<string> modes)
            {
                List<char> deviceLetters = new List<char>();
                foreach (string mode in modes)
                {
                    foreach (PickEngineDevice pickEngineDevice in this)
                    {
                        Device device = pickEngineDevice.Device;
                        if (device.Mode != mode)
                            continue;

                        foreach (char sourceDevice in device.SourceDevice)
                        {
                            if (deviceLetters.Contains(sourceDevice) == false)
                                deviceLetters.Add(sourceDevice);
                        }
                    }
                }
                return deviceLetters.ToArray();
            }
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;

namespace PickData {
    public class PickPrefix {
        private Guid _rid;
        private string _id;
        private string _data;
        private string _description;

        public Guid Rid
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public class List : List<PickPrefix> {}
    }
}

using System.Collections.Generic;

namespace PickData {
    public class PickDevice {
        private string _id;
        private int _tn;

        private int _rid;
        private string _brand;
        private int _brandCode;
        private string _manufacturer;
        private int _manufacturerCode;
        private string _ueiProduct;
        private int _ueiProductCode;

        private string _model;
        private string _branch;
        private string _date;
        private string _collector;
        private string _purchaseYear;

        private string _priceInDigit;
        private string _productManual;
        private bool _isCodebook;
        private bool _isTarget;
        private bool _isPartNumber;

        private bool _isRetail;
        private string _sourceRid;
        private string _text;

        private PickCountry.List _countryList;
        private PickDeviceType.List _deviceTypeList;
//        private OpInfoCollection _opInfoList;

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public int Rid
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public string Brand
        {
            get { return _brand; }
            set { _brand = value; }
        }

        public int BrandCode
        {
            get { return _brandCode; }
            set { _brandCode = value; }
        }

        public string Manufacturer
        {
            get { return _manufacturer; }
            set { _manufacturer = value; }
        }

        public int ManufacturerCode
        {
            get { return _manufacturerCode; }
            set { _manufacturerCode = value; }
        }

        public string UEIProduct
        {
            get { return _ueiProduct; }
            set { _ueiProduct = value; }
        }

        public int UEIProductCode
        {
            get { return _ueiProductCode; }
            set { _ueiProductCode = value; }
        }

        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        public string Branch
        {
            get { return _branch; }
            set { _branch = value; }
        }

        public string Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public string Collector
        {
            get { return _collector; }
            set { _collector = value; }
        }

        public string PurchaseYear
        {
            get { return _purchaseYear; }
            set { _purchaseYear = value; }
        }

        public string PriceInDigit
        {
            get { return _priceInDigit; }
            set { _priceInDigit = value; }
        }

        public string ProductManual
        {
            get { return _productManual; }
            set { _productManual = value; }
        }

        public bool IsCodebook
        {
            get { return _isCodebook; }
            set { _isCodebook = value; }
        }

        public bool IsTarget
        {
            get { return _isTarget; }
            set { _isTarget = value; }
        }

        public bool IsPartNumber
        {
            get { return _isPartNumber; }
            set { _isPartNumber = value; }
        }

        public bool IsRetail
        {
            get { return _isRetail; }
            set { _isRetail = value; }
        }

        public string SourceRid
        {
            get { return _sourceRid; }
            set { _sourceRid = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public PickCountry.List CountryList
        {
            get
            {
                if (_countryList == null)
                    _countryList = new PickCountry.List();

                return _countryList;
            }
            set { _countryList = value; }
        }

        public PickDeviceType.List DeviceTypeList
        {
            get
            {
                if (_deviceTypeList == null)
                    _deviceTypeList = new PickDeviceType.List();

                return _deviceTypeList;
            }
            set { _deviceTypeList = value; }
        }

        public class List : List<PickDevice> { }
    }
}

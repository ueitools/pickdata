using System.Collections.Generic;

namespace PickData {
    public class PickDeviceType {
        private int _dr_rid;
        private int _code;
        private string _name;

        public int DrRid
        {
            get { return _dr_rid; }
            set { _dr_rid = value; }
        }

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public class List : List<PickDeviceType> {}
    }
}

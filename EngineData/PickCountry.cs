using System;
using System.Collections.Generic;

namespace PickData {
    public class PickCountry {
        private int _drRid;
        private int _code;
        private string _region;
        private string _name;

        public int DrRid
        {
            get { return _drRid; }
            set { _drRid = value; }
        }

        public int Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Region
        {
            get { return _region; }
            set { _region = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public override string ToString()
        {
            return Region;
        }

        public class List : List<PickCountry> {}
    }
}

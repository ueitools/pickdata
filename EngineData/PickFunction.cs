using System;
using System.Collections.Generic;

namespace PickData {
    public class PickFunction {
        private string _label;
        private string _intron;
        private string _intronPriority;
        private string _data;
        private string _comment;

        public string Label
        {
            get { return _label; }
            set
            {
                _label = string.Empty;
                if (value != null)
                    _label = value;
            }
        }

        public string Intron
        {
            get { return _intron; }
            set
            {
                _intron = string.Empty;
                if (value != null)
                    _intron = value;
            }
        }

        public string IntronPriority
        {
            get { return _intronPriority; }
            set
            {
                _intronPriority = string.Empty;
                if (value != null)
                    _intronPriority = value;
            }
        }

        public int IntronPriorityKey
        {
            get
            {
                const int LAST_INTRON_PRIORITY = 9999;

                int intronPriority;
                if (int.TryParse(_intronPriority, out intronPriority))
                    return intronPriority;

                return LAST_INTRON_PRIORITY;
            }
        }

        public string Data
        {
            get { return _data; }
            set
            {
                _data = string.Empty;
                if (value != null)
                    _data = value;
            }
        }

        public string Comment
        {
            get { return _comment; }
            set
            {
                _comment = string.Empty;
                if (value != null)
                    _comment = value;
            }
        }

        public override string ToString()
        {
            return Label;
        }

        public class List : List<PickFunction>
        {
            public string MakeDataValid(string data)
            {
                int dataLength = 0;
                foreach (PickFunction pickFunction in this)
                {
                    if (string.IsNullOrEmpty(pickFunction.Data))
                        continue;

                    dataLength = Math.Max(dataLength, pickFunction.Data.Length);
                }

                string validData = data.PadLeft(dataLength, '0');
                if (dataLength < data.Length)
                    validData = validData.Substring(data.Length - dataLength);

                return validData;
            }

        }
    }
}

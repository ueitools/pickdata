using System;
using System.Collections.Generic;

namespace PickData {
    [Serializable]
    public class PickId {
        private PickIdHeader _header;
        private PickFunction.List _functionList;
        private PickDevice.List _deviceList;

        private bool _moved;

        public PickIdHeader Header
        {
            get
            {
                if (_header == null) 
                    _header = new PickIdHeader();

                return _header;
            }
            set { _header = value; }
        }

        public PickFunction.List FunctionList
        {
            get { return _functionList; }
            set { _functionList = value; }
        }

        public PickDevice.List DeviceList
        {
            get
            {
                if (_deviceList == null)
                    _deviceList = new PickDevice.List();

                return _deviceList;
            }
            set { _deviceList = value; }
        }

        public bool HasBeenMoved
        {
            get {
                return _moved;
            }
        }

        public void SetAsMoved()
        {
            _moved = true;
        }

        public override string ToString()
        {
            return Header.ID;
        }

        public class List : List<PickId> {
            public PickId GetID(string idName)
            {
                return Find(delegate(PickId pickID) { return pickID._header.ID == idName; });
            }

            public bool Contains(string id)
            {
                return (GetID(id) != null);
            }
        }
    }
}

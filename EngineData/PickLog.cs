using System;
using System.Collections.Generic;

namespace PickData {
    public class PickLog {
        int _rid;
        int _tn;
        string _id;
        DateTime _time;
        string _operator;
        string _content;

        public int Rid
        {
            get { return _rid; }
            set { _rid = value; }
        }

        public int TN
        {
            get { return _tn; }
            set { _tn = value; }
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public string Operator
        {
            get { return _operator; }
            set { _operator = value; }
        }

        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        public class List : List<PickLog> { }
    }
}

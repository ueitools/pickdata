using System;

namespace PickData {
    [Serializable]
    public class PickIdHeader {
        public enum StatusFlag {
            Shadow,
            Modified,
            PassedQA,
            ExecHold,
            ProjectHold
        }

        private string _id;
        private string _parentID;
        private int _executorCode = -1;
        private StatusFlag _status = StatusFlag.Modified;
        private bool _isInversedData;

        private bool _isExternalPrefix;
        private bool _isRestricted;
        private bool _isFrequencyData;
        private bool _isInversedPrefix;
        private bool _isHexFormat;

        private string _text;
        private bool _isLocked;

        private PickPrefix.List _prefixList;
        private PickLog.List _logList;

        public PickIdHeader()
        {
            _prefixList = new PickPrefix.List();
            _logList = new PickLog.List();
        }

        public string ID
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ParentID
        {
            get { return _parentID; }
            set { _parentID = value; }
        }

        public int ExecutorCode
        {
            get { return _executorCode; }
            set { _executorCode = value; }
        }

        public StatusFlag Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public bool IsInversedData
        {
            get { return _isInversedData; }
            set { _isInversedData = value; }
        }

        public bool IsExternalPrefix
        {
            get { return _isExternalPrefix; }
            set { _isExternalPrefix = value; }
        }

        public bool IsRestricted
        {
            get { return _isRestricted; }
            set { _isRestricted = value; }
        }

        public bool IsFrequencyData
        {
            get { return _isFrequencyData; }
            set { _isFrequencyData = value; }
        }

        public bool IsInversedPrefix
        {
            get { return _isInversedPrefix; }
            set { _isInversedPrefix = value; }
        }

        public bool IsHexFormat
        {
            get { return _isHexFormat; }
            set { _isHexFormat = value; }
        }

        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        public bool IsLocked
        {
            get { return _isLocked; }
            set { _isLocked = value; }
        }

        public PickPrefix.List PrefixList
        {
            get { return _prefixList; }
            set { _prefixList = value; }
        }

        public PickLog.List LogList
        {
            get { return _logList; }
            set { _logList = value; }
        }
    }
}

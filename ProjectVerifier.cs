
using System.Collections.Generic;
using System.Windows.Forms;

namespace PickData
{
    public class ProjectVerifier
    {
        public bool ValidateIdUsage(List<string> ids, Project project,
                                    ref string idsNotCovered, ref string deviceNotCovered)
        {
            bool result = true;
            List<char> devices = new List<char>();
            foreach (string id in ids)
            {
                if (!devices.Contains(id[0]))
                    devices.Add(id[0]);
            }

            foreach (Device device in project.DeviceList)
            {
                if (!devices.Contains(device.DeviceName))
                    deviceNotCovered += device.DeviceName;
            }

            foreach (char device in devices)
            {
                bool found = false;
                foreach (Device item in project.DeviceList)
                {
                    if (item.DeviceName == device)
                    {
                        found = true;
                        break;
                    }
                }
                if (found) continue;
                result = false;
                idsNotCovered += device;
            }
            return result;
        }

        public DialogResult CommonIdUsageResultDialog(string idsNotCovered, string deviceNotCovered)
        {
            string msg = "There are IDs in the load that are not accessable by any Devices";
            const string fmt = ": {0}\r\n\t\n";
            msg += string.Format(fmt, idsNotCovered);

            if (!string.IsNullOrEmpty(deviceNotCovered))
            {
                if (!string.IsNullOrEmpty(idsNotCovered))
                    msg += "and ";
                msg += string.Format("There are Configured Devices which have no IDs in the load");
                msg += string.Format(fmt, deviceNotCovered);
            }
            return MessageBox.Show(msg + "Continue Anyway?", "Warning", MessageBoxButtons.YesNo);
        }

        public bool ValidateModeKeyLists(Project project)
        {
            string report = "";
            foreach (Mode mode in project.ModeList)
            {
                Device.List deviceList = mode.GetDeviceList(mode);
                if (deviceList.Count < 2)
                    continue;
                foreach (Key key in project.KeyList)
                {
                    int fwlCount = 0;
                    foreach (Device device in deviceList)
                    {
                        if (!string.IsNullOrEmpty(device.DeviceKeyList[key.KeyIndex].FirmwareLabel))
                            fwlCount++;
                    }
                    if (fwlCount > 0 && fwlCount < deviceList.Count)
                    {
                        report += string.Format("\tMode: {0}\t Devices: {1}\t \r\n", mode.Name, deviceList);
                        break;
                    }
                }
            }
            if (string.IsNullOrEmpty(report))
                return true;

            report = "Key Distribution Errors Were Detected:\r\n\r\n" + report + 
                     "\r\nPress OK to Auto Correct and Continue or Cancel to Abort";

            if (MessageBox.Show(report, "Error", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                AutoCorrectModeKeyDistribution(project);
                return true;
            }
            return false;
        }

        private void AutoCorrectModeKeyDistribution(Project project)
        {
            foreach (Key key in project.KeyList)
            {
                foreach (Mode mode in project.ModeList)
                {
                    Device.List deviceList = mode.GetDeviceList(mode);
                    if (deviceList.Count < 2)
                        continue;
                    int fwlCount = 0;
                    foreach (Device device in deviceList)
                    {
                        if (!string.IsNullOrEmpty(device.DeviceKeyList[key.KeyIndex].FirmwareLabel))
                            fwlCount++;
                    }
                    if (fwlCount == 0 || fwlCount >= deviceList.Count) 
                        continue;
                    foreach (Device device in deviceList)
                    {
                        if (string.IsNullOrEmpty(device.DeviceKeyList[key.KeyIndex].FirmwareLabel))
                            device.DeviceKeyList[key.KeyIndex].FirmwareLabel = key.FirmwareLabel;
                    }
                }
            }
        }
    }
}

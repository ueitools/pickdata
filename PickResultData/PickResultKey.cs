using System;
using System.Collections.Generic;

namespace PickData {
    public class PickResultKey {
        private DeviceKey _deviceKey;

        private PickFunction _pickFunction;
        private string _data;
        private string _label;
        private string _intron;
        private string _linkKey;

        public PickResultKey()
        {
            Initialize();
        }

        public PickResultKey(PickResultKey pickResultKey)
        {
            Initialize();

            _deviceKey = pickResultKey._deviceKey;
            _pickFunction = pickResultKey._pickFunction;
            _data = pickResultKey._data;
            _label = pickResultKey._label;
            _intron = pickResultKey._intron;
            _linkKey = pickResultKey._linkKey;
        }

        public PickResultKey(DeviceKey deviceKey)
        {
            Initialize();

            _deviceKey = deviceKey;
        }

        private void Initialize()
        {
            _data = string.Empty;
            _label = string.Empty;
            _intron = string.Empty;
        }

        public DeviceKey DeviceKey
        {
            set { _deviceKey = value; }
            get { return _deviceKey; }
        }

        public string Intron
        {
            get
            {
                if (_pickFunction != null)
                    return _pickFunction.Intron;

                return _intron;
            }
        }

        public string Data {
            get
            {
                if (_pickFunction != null)
                    return _pickFunction.Data;

                return _data;
            }
            set
            {
                _data = value;
                _pickFunction = null;
            }
        }

        public string FirmwareLabel
        {
            get
            {
                if (_deviceKey == null)
                    return string.Empty;

                return _deviceKey.FirmwareLabel;
            }
        }

        public string Comment
        {
            get
            {
                if (_pickFunction == null)
                    return string.Empty;

                return _pickFunction.Comment;   
            }
        }

        public string Label
        {
            get
            {
                if (_pickFunction != null)
                    return _pickFunction.Label;

                return _label;
            }
            set
            {
                _label = value;
                _pickFunction = null;
            }
        }

        public string IntronPriority
        {
            get
            {
                if (_pickFunction == null)
                    return string.Empty;

                return _pickFunction.IntronPriority;
            }
        }

        public string LinkKey
        {
            get { return _linkKey; }
        }

        public bool IsLinkedKey
        {
            get { return string.IsNullOrEmpty(_linkKey) == false; }
        }

        public bool IsDataAssigned
        {
            get { return string.IsNullOrEmpty(Data) == false; }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1})", Intron, Data);
        }

        public void SetPickFunction(PickFunction pickFunction)
        {
            _pickFunction = pickFunction;
            _data = string.Empty;
        }

        public void SetLinkKey(string linkKey)
        {
            _linkKey = linkKey;
        }

        public class List : List<PickResultKey> {
            public int GetLongestColumnCount()
            {
                int longestValue = 0;
                foreach (PickResultKey pickResultKey in this)
                {
                    longestValue = Math.Max(longestValue, pickResultKey.DeviceKey.IntronList.Count);
                }
                return longestValue;
            }

            public PickResultKey FindKey(string linkKey)
            {
                foreach (PickResultKey pickResultKey in this)
                {
                    if (pickResultKey.DeviceKey.FirmwareLabel == linkKey)
                        return pickResultKey;
                }
                return null;
            }

            public List Clone()
            {
                List resultList = new List();
                foreach (PickResultKey resultKey in this)
                {
                    PickResultKey pickResultKey = new PickResultKey(resultKey);
                    resultList.Add(pickResultKey);
                }
                return resultList;
            }
        }

        public void CopyPickFunction(PickResultKey sourceResultKey)
        {
            _pickFunction = sourceResultKey._pickFunction;
        }

        public void SetNonFunctional(string nopData)
        {
            Data = nopData;
            _label = "Non-functional";
            _intron = "G.UN";
        }
    }
}

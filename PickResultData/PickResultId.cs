using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace PickData {
    [Serializable]
    public class PickResultId {
        private string _pickId;
        private PickResultKey.List _pickResultKeyList;
        private PickId _originalPickId;

        public PickResultId()
        {            
        }

        public PickResultId(string id, PickId pickID)
        {
            _pickId = id;
            _originalPickId = pickID;
            _pickResultKeyList = new PickResultKey.List();
        }

        public PickResultId(string id, PickId pickID, PickResultKey.List pickResultKeyList)
        {
            _pickId = id;
            _originalPickId = pickID;
            _pickResultKeyList = pickResultKeyList;
        }

        public string Id
        {
            get { return _pickId; }
        }

        public PickResultKey.List KeyList
        {
            get { return _pickResultKeyList; }
        }

        public PickId OriginalPickID
        {
            get { return _originalPickId; }
        }

        public void CreatePickResultKeys(DeviceKey.List deviceKeys)
        {
            KeyList.Clear();
            foreach (DeviceKey key in deviceKeys)
            {
                PickResultKey pickResultKey = new PickResultKey(key);
                KeyList.Add(pickResultKey);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} ({1} keys)", _pickId, _pickResultKeyList.Count);
        }

        public string ToXml()
        {
            StringWriter stringWriter = new StringWriter();
            XmlSerializer xPrefix = new XmlSerializer(OriginalPickID.Header.GetType());
            xPrefix.Serialize(stringWriter, OriginalPickID.Header);
            XmlSerializer xFunction = new XmlSerializer(this.GetType());
            xFunction.Serialize(stringWriter, this);
            stringWriter.Close();

            return stringWriter.ToString();
        }

        public class List : List<PickResultId>
        {
        }
    }
}

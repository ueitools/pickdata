namespace PickData
{
    public interface IPickWriter
    {
        void WritePickConfig(string projectName, IProject config);
        void Write(string projectName, PickResultId.List pickResultId);
    }
}

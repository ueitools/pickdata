using System;
using System.Collections.Generic;
using System.Text;

namespace PickData.ConfigurationData
{
    [Serializable]
    public class FirmwareKeyGroups : List<string>
    {
        public FirmwareKeyGroups()
        {
        }

        public bool IsModeKey()
        {
            return HasKeyProperty("Device Key") || HasKeyProperty("KG_MODES");
        }

        public bool IsIRKey()
        {
            return HasKeyProperty("IR Key");
        }

        public bool IsNonMacroKey()
        {
            return HasKeyProperty("Non-Macro Key") || HasKeyProperty("KG_NON_MACRO");
        }

        public bool IsRotatingMacroKey()
        {
            return HasKeyProperty("Rotating Macro Key") || HasKeyProperty("KG_ROTATE_MACRO");
        }

        public bool IsNonGeneralPunchThroughProperty()
        {
            return HasKeyProperty("Non General IR Punch Through") || HasKeyProperty("KG_NON_GENERAL_IR_PT");
        }

        public bool IsRFIDAction()
        {
            return HasKeyProperty("KG_ACTION");
        }

        public bool IsRFIDMode()
        {
            return HasKeyProperty("KG_RFIDMODE");
        }

        private bool HasKeyProperty(string property)
        {
            return this.Exists(
                delegate(string item)
                {
                    const bool ignoreCase = true;
                    return string.Compare(item.Trim(), property, ignoreCase) == 0;
                });            
        }

        /// <summary>
        /// note that some keys may have the same mode list. 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="project"></param>
        /// <returns></returns>
        /// 
        public bool IsModeKey(char mode, string firmwareLabel, IProject project)
        {
            if (!IsModeKey())
                return false;

            int deviceNumber;
            if (!FindDeviceNumber(out deviceNumber))
                return false;

            bool found = false;

            if (deviceNumber >= project.ModeList.Count)
                return false;

            foreach (PickData.Device device in project.ModeList[deviceNumber].DeviceList)
            {
                found = device.DeviceName == mode;
                if (found)
                    break;
            }

            return found;
        }

        public bool HasValidDeviceNumberIfExists(int deviceCount)
        {
            int deviceTypeNumber = 0;

            if (!FindDeviceNumber(out deviceTypeNumber))
                return true;
            
            return deviceTypeNumber < deviceCount;
        }

        public bool FindDeviceNumber(out int deviceTypeNumber)
        {
            const string specialGroup = "KG_MODEORDER_";

            deviceTypeNumber = 0;
            bool found = false;

            List<string> properties = this.FindAll(
                delegate(string item)
                {
                    const bool ignoreCase = true;
                    return item.ToUpper().Trim().StartsWith(specialGroup);
                });

            found = properties.Count == 1;

            if (properties.Count > 1)
                throw new ArgumentException("Too many mode numbers in key.");
            else if (found)
            {
                deviceTypeNumber = int.Parse(properties[0].Substring(specialGroup.Length));
            }

            return found;
        }

        public bool IsDeviceTypeNumbersKey(string firmwareLabel, int deviceTypeNum, IProject project)
        {
            if (!IsModeKey())
                return false;

            int keyDeviceTypeNumber;
            bool found = FindDeviceNumber(out keyDeviceTypeNumber);

            return found && (keyDeviceTypeNumber == deviceTypeNum);
        }
    }
}

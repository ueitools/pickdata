using System;
using System.ComponentModel;
using CommonCode;

namespace PickData
{
    [Serializable]
    public class IntronData
    {
        public enum Type
        {
            Customer,
            Intron,
            Link,
            Data
        }

        private string _customerData;
        private Data _data;
        private string _intron;
        private string _link;
        private string _label;
        private Type _intronType = Type.Intron;

        [Category("Intron Settings")]
        public Type IntronType
        {
            get { return _intronType; }
            set { _intronType = value; }
        }

        [Category("Intron Settings")]
        public string Intron
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _intron = null;
                    return;
                }
                _intronType = Type.Intron;
                _intron = value.Trim();
            }
            get { return _intron; }
        }

        [Category("Intron Settings")]
        public string Data
        {
            set
            {
                if (string.IsNullOrEmpty(value) == false)
                {
                    foreach (char c in value)
                        if (c != '0' && c != '1')
                            return;

                    _intronType = Type.Data;
                    if (_data == null)
                        _data = new Data();
                    _data.Bin = value.Trim();
                }
                else
                    _data = null;
            }
            get
            {
                if (_data == null || string.IsNullOrEmpty(_data.Bin))
                    return null;
                string result = _data.Bin;
                for (int i = 0; i < result.Length % 8; i++)
                    result = "0" + result;
                return result;
            }
        }

        [Category("Intron Settings")]
        public string Label
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _label = "";
                    return;
                }
                _label = value.Trim();
            }
            get { return _label; }
        }

        [Category("Intron Settings")]
        public string CustomerData
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _customerData = "";
                    return;
                }
                _customerData = value.Trim();
            }
            get { return _customerData; }
        }

        [Category("Intron Settings")]
        public string Link
        {
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _link = "";
                    return;
                }
                _intronType = Type.Link;
                _link = value.Trim();
            }
            get { return _link; }
        }

        public IntronData() {}

        public IntronData(IntronData data)
        {
            _customerData = data._customerData;
            _data = data._data;
            _intron = data._intron;
            _link = data._link;
            _label = data._label;
            _intronType = data._intronType;
        }

        public IntronData(TemplateItem value)
        {
            Intron = value.Intron;
            Label = value.KeyName;
            Data = value.Data;
            CustomerData = value.CustomerData;
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PickData
{
    /// <summary>
    /// Device Key
    /// This is different from a system Key but an existing system key must refer to the Device Key
    /// </summary>
    [Serializable]
    public class DeviceKey : IComparable<DeviceKey>
    {
        private string _label;
        private string _firmwareLabel;
        private List<IntronData> _intronList = new List<IntronData>();
        private string _keySpecNumber;
        private int _groupNumber;
        private int _bitmapGroup = -1;
        private int _keyIndex;
        private Key.KeyType _type;
        private bool _disallowDupes = false;
        private bool _allowIntronDupes = false;

        #region Accessors

        /// <summary>
        /// Key Label
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public string Label
        {
            set { _label = value; }
            get { return _label; }
        }

        /// <summary>
        /// firmware label
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public string FirmwareLabel
        {
            set { _firmwareLabel = ((string.IsNullOrEmpty(value))?"":value.ToLower()); }
            get { return _firmwareLabel; }
        }

        /// <summary>
        /// Key number accroding to project specification
        /// </summary>
        [Browsable(Common.debugOnly)]
        public string KeySpecNumber
        {
            set { _keySpecNumber = value; }
            get { return _keySpecNumber; }
        }

        /// <summary>
        /// Group Pick number
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public int GroupNumber
        {
            get { return _groupNumber; }
            set { _groupNumber = value; }
        }

        /// <summary>
        /// Bitmap group number
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public int BitmapGroup
        {
            get { return _bitmapGroup; }
            set { _bitmapGroup = value; }
        }

        /// <summary>
        /// PGS display order 
        /// </summary>
        [Browsable(Common.debugOnly)]
        public int KeyIndex         // still useful?  should always be the same as key speck number
        {
            set { _keyIndex = value; }
            get { return _keyIndex; }
        }

        /// <summary>
        /// list of assigned introns
        /// </summary>
        [Browsable(Common.debugOnly)]
        public List<IntronData> IntronList
        {
            set { _intronList = value; }
            get { return _intronList; }
        }

        /// <summary>
        /// Key Attribute (standard key, virtual key...)
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public Key.KeyType Type
        {
            set { _type = value;}
            get { return _type; }
        }

        /// <summary>
        /// Disallow dupes
        /// </summary>
        [CategoryAttribute("Device Key Settings")]
        public bool DisallowDupes
        {
            get { return _disallowDupes; }
            set { _disallowDupes = value; }
        }

        /// <summary>
        /// Allow Intron Dupes
        /// </summary>
        [CategoryAttribute ( "Device Key Settings" )]
        public bool AllowIntronDupes
        {
            get
            {
                return _allowIntronDupes;
            }
            set
            {
                _allowIntronDupes = value;
            }
        }

        /// <summary>
        /// KeySpecView
        /// </summary>
        [Browsable(Common.debugOnly)]
        public string KeySpecView
        {
            get { return Key.Prefix(Type) + (KeySpecNumber);}
        }
#endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        int IComparable<DeviceKey>.CompareTo(DeviceKey other)
        {
            if (KeyIndex < other.KeyIndex) return -1;
            if (KeyIndex > other.KeyIndex) return 1;
            return 0;
        }

        /// <summary>
        /// default constructor
        /// </summary>
        public DeviceKey() { }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="key">existing DeviceKey</param>
        public DeviceKey(DeviceKey key)
        {
            Label = key.Label;
            FirmwareLabel = key.FirmwareLabel;
            KeySpecNumber = key.KeySpecNumber;
            GroupNumber = key.GroupNumber;
            BitmapGroup = key.BitmapGroup;
            KeyIndex = key.KeyIndex;
            Type = key.Type;
            DisallowDupes = key.DisallowDupes;
            foreach (IntronData item in key.IntronList)
            {
                IntronList.Add(item);
            }
        }

        /// <summary>
        /// Create a new Device key based on a Project Key
        /// </summary>
        /// <param name="key">Project Key</param>
        public DeviceKey(Key key)
        {
            Label = key.Name;
            KeySpecNumber = key.KeySpecNumber;
            KeyIndex = key.KeyIndex;
            Type = key.Type;
            GroupNumber = Common.NoGroup;
        }

        public DeviceKey ( Key key, string specno )
            {
                Label = key.Name;
                KeySpecNumber = specno;
                KeyIndex = key.KeyIndex;
                Type = key.Type;
                GroupNumber = Common.NoGroup;
            }

        /// <summary>
        /// Create a new Device key based on a Template Item
        /// </summary>
        /// <param name="value">Key Dictionary Template Item</param>
        /// <param name="keyNumber">Key Number</param>
        public DeviceKey(TemplateItem value, int keyNumber)
        {
            Label = value.KeyName;
            FirmwareLabel = value.FirmwareLabel;
            KeyIndex = keyNumber;
            KeySpecNumber = (keyNumber + 1).ToString();
            Type = Key.KeyType.Standard;
            GroupNumber = Common.NoGroup;

        }

        /// <summary>
        /// Allocate an Intron to an existing key
        /// </summary>
        /// <param name="intronIndex">Column to add intron</param>
        /// <param name="item">Intron Data from intron dictionary</param>
        public void AllocateIntron(int intronIndex, IntronData item)
        {
            if (intronIndex >= IntronList.Count)
                IntronList.Add(item);
            else
                IntronList.Insert(intronIndex, item);
                //IntronList[intronIndex] = item;
        }

        /// <summary>
        /// Remove an Intron from an intron allocation list
        /// </summary>
        /// <param name="intronIndex"></param>
        public void RemoveIntron(int intronIndex)
        {
            if (intronIndex >= 0 && intronIndex < IntronList.Count)
                IntronList.RemoveAt(intronIndex);
        }

        /// <summary>
        /// DeviceKey List
        /// </summary>
        [Serializable]
        public class List : List<DeviceKey>
        {
            public bool HasKeyAssignments()
            {
                foreach (DeviceKey deviceKey in this)
                {
                    if (string.IsNullOrEmpty(deviceKey.FirmwareLabel) == false)
                        return true;
                }
                return false;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using PickData.ConfigurationData;

namespace PickData
{
    [Serializable]
    public class Key : IComparable<Key>
    {
        private string _name;
        private KeyType _type;
        private string _keySpecNumber;
        private string _firmwareLabel;  // link to Pick Template
        private int _scancode;
        private int _keyIndex;
        private FirmwareKeyGroups _groupMembership;

        public enum KeyType
        {
            Standard,
            Shifted,
            Virtual,
            CAP,
            ALT,
            PlaceHolder
        }
        public static string Prefix(KeyType t)
        {
            switch (t)
            {
                case KeyType.Shifted:
                    return "SK";
                case KeyType.Virtual:
                    return "VK";
                case KeyType.CAP:
                    return "CK";
                case KeyType.ALT:
                    return "AK";
                case KeyType.PlaceHolder:
                    return "Z";
                default:
                    return "K";
            }
        }
#region Accessors
        [CategoryAttribute("Key Attributes"),
        DescriptionAttribute("Physical Key Name (Plastic Label)")]
        public string Name
        {
            set { _name = value; }
            get { return _name; }
        }

        [CategoryAttribute("Key Attributes"),
        DescriptionAttribute("Key Type, Standard/Shifted/Virtual/CAP/ALT"),
        ReadOnly(true)]
        public KeyType Type
        {
            set { _type = value; }
            get { return _type; }
        }

        //[CategoryAttribute("Key Attributes"),
        //DescriptionAttribute("Key Number usually the same as is found on the product specification")]
        [Browsable(Common.debugOnly)]
        public string KeySpecNumber
        {
            set { _keySpecNumber = value; }
            get { return _keySpecNumber; }
        }

        [Browsable(Common.debugOnly)]
        public string KeySpecView
        {
            get { return Prefix(Type) + (KeySpecNumber);}
        }

        [CategoryAttribute("Key Attributes"),
        DescriptionAttribute("Associated Firmware Label")]
        public string FirmwareLabel
        {
            set { _firmwareLabel = value.ToLower(); }
            get { return _firmwareLabel; }
        }

        [Browsable(Common.debugOnly)]
        public int KeyIndex
        {
            get { return _keyIndex; }
            set { _keyIndex = value; }
        }

        [CategoryAttribute("Key Attributes"),
        DescriptionAttribute("Physical Scan Code for the key")]
        public int Scancode
        {
            set { _scancode = value; }
            get { return _scancode; }
        }

        [CategoryAttribute("Key Attributes"),
        DescriptionAttribute("SRI file key group membership (Firmware)")]
        [Editor(@"System.Windows.Forms.Design.StringCollectionEditor, 
                System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a",
                typeof(System.Drawing.Design.UITypeEditor))]
        public FirmwareKeyGroups GroupMembership
        {
            set { _groupMembership = value; }
            get { return _groupMembership; }
        }

#endregion

        public Key(){}

        public Key(TemplateItem value, int keyNumber, KeyType type)
        {
            Name = value.KeyName;
            Type = type;
            if (type == KeyType.Standard)
                KeySpecNumber = (keyNumber + 1).ToString();
            else
                KeySpecNumber = (value.SortingOrder + 1).ToString();
            KeyIndex = keyNumber;
            FirmwareLabel = value.FirmwareLabel;
        }

        #region Code to handle shifting key for ALT,CAP, SHIFT keys
        public Key ( TemplateItem value, int keyNumber, KeyType type,int _specno )
        {
            Name = value.KeyName;
            Type = type;
            if ( type == KeyType.Standard )
                KeySpecNumber = ( keyNumber + 1 ).ToString ( );
            else
                KeySpecNumber = _specno.ToString ( );
            KeyIndex = keyNumber;
            FirmwareLabel = value.FirmwareLabel;
        }

        #endregion

        int IComparable<Key>.CompareTo(Key other)
        {
            if (KeyIndex < other.KeyIndex) return -1;
            if (KeyIndex > other.KeyIndex) return 1;
            return 0;
        }

        [Serializable]
        public class List : List<Key> { }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PickData
{
    [Serializable]
    [XmlInclude(typeof(StandardDevice))]
    [XmlInclude(typeof(SubDevice))]
    [XmlInclude(typeof(MarketDevice))]
    [XmlInclude(typeof(IdDevice))]
    [XmlInclude(typeof(TnDevice))]
    [XmlInclude(typeof(FullCopyDevice))]
    [XmlInclude(typeof(SubGroupDevice))]
    public abstract class Device : IComparable<Device>
    {
        protected char _device;
        private List _deviceList = new List();
        private int _deviceOrder;
        private readonly List<string> _modeList = new List<string>(); // DO NOT USE. Deprecated - ModeList is here for compatibility ONLY.
        protected string _modeSource;
        protected bool _move;
        private GroupPickParameters.List _groupParametersList = new GroupPickParameters.List();
        private DeviceKey.List _deviceKeyList = new DeviceKey.List();

#region Accessors
    
        [CategoryAttribute("Device Settings"),
        DescriptionAttribute("The single character Device letter")]
        public char DeviceName
        {
            get { return _device; }
            set { _device = value; }
        }

        [CategoryAttribute("Device Settings"),
        DescriptionAttribute("The list of Devices (if any) that were created under this device")]
        public List DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }

        [Browsable(Common.debugOnly)]
        public int DeviceOrder
        {
            get { return _deviceOrder; }
            set { _deviceOrder = value; }
        }

        [CategoryAttribute("Device Settings"),
        DescriptionAttribute("The name of the Mode this device is part of")]
        public string Mode
        {
            get
            {
                if (_modeSource == null && _modeList.Count > 0)
                    _modeSource = _modeList[0];
                _modeList.Clear();
                return _modeSource;
            }
            set
            {
                _modeSource = value;
                if (DeviceList.Count > 0)
                    foreach (Device device in DeviceList)
                        device.Mode = value;
            }
        }

        /// <summary>
        /// DO NOT USE. Deprecated - here for compatibility ONLY, use Mode
        /// </summary>
        [Browsable(Common.debugOnly)]
        public List<string> ModeList {get{return _modeList;}}

        [CategoryAttribute("Device Settings"),
        DescriptionAttribute("Tells if the IDs in this device were Moved or Copied from it's source")]
        public bool Move
        {
            get { return _move; }
            set { _move = value; }
        }

        [Browsable(Common.debugOnly)]
        public DeviceKey.List DeviceKeyList
        {
            set { _deviceKeyList = value; }
            get { return _deviceKeyList; }
        }

        [Browsable(Common.debugOnly)]
        public GroupPickParameters.List GroupParametersList
        {
            get { return _groupParametersList; }
            set { _groupParametersList = value; }
        }


        [CategoryAttribute("Device Settings"),
        DescriptionAttribute("A list of Devices this Device is Sourced from")]
        public abstract List<char> SourceDevice { get; set;}
        [Browsable(Common.debugOnly)]
        public abstract bool RequiresExactMatch { get; }
        [Browsable(Common.debugOnly)]
        public abstract int SortingWeight { get; }
        [Browsable(Common.debugOnly)]
        public abstract bool IsCustom { get; }

        #endregion

        int IComparable<Device>.CompareTo(Device other)
        {
            if (DeviceOrder < other.DeviceOrder) return -1;
            if (DeviceOrder > other.DeviceOrder) return 1;
            return 0;
        }

        public bool DeviceKeysContains(string firmwareLabel)
        {
            foreach (DeviceKey key in DeviceKeyList)
            {
                if (key.FirmwareLabel == firmwareLabel)
                    return true;
            }
            return false;
        }

        public DeviceKey FindDeviceKey(string firmwareLabel)
        {
            foreach (DeviceKey key in DeviceKeyList)
            {
                if (key.FirmwareLabel == firmwareLabel)
                    return key;
            }
            return null;
        }

        public DeviceKey GetDeviceKey(int keyNumber)
        {
            foreach (DeviceKey key in DeviceKeyList)
            {
                if (key.KeyIndex == keyNumber)
                    return key;
            }
            return null;
        }

        public void AllocateDeviceKey(DeviceKey key)
        {
            AllocateDeviceKey(this, key);
        }

        public void AllocateDeviceKey(Device dev, DeviceKey key)
        {
            DeviceKey item = dev.FindDeviceKey(key.FirmwareLabel);
            if (item != null && item.KeyIndex != key.KeyIndex)
            {
                throw new Exception("Label has already been allocated");
            }

            DeviceKey deviceKey = dev.GetDeviceKey(key.KeyIndex);
            if (deviceKey != null)
            {
                deviceKey.FirmwareLabel = key.FirmwareLabel;
                deviceKey.KeyIndex = key.KeyIndex;
            }
            else
                dev.DeviceKeyList.Add(key);

            if (dev.DeviceList.Count > 0)
                foreach (Device subdev in dev.DeviceList)
                {
                    AllocateDeviceKey(subdev, key);
                }
        }

        public void InsertDeviceKey(Key key, int keyNumber)
        {
            InsertDeviceKey(this, key, keyNumber);
        }

        private void InsertDeviceKey(Device dev, Key key, int keyNumber)
        {
            foreach (DeviceKey deviceKey in dev.DeviceKeyList)
            {
                if (deviceKey.KeyIndex >= keyNumber)
                    deviceKey.KeyIndex++;
            }
            dev.DeviceKeyList.Insert(keyNumber, new DeviceKey(key));
            if (dev.DeviceList.Count > 0)
                foreach (Device subdev in dev.DeviceList)
                {
                    InsertDeviceKey(subdev, key, keyNumber);
                }
        }

        public void UpdateDeviceKeys(Key key, bool updateFirmwareLabel)
        {
            if (updateFirmwareLabel)
                DeviceKeyList[key.KeyIndex].FirmwareLabel = key.FirmwareLabel;
            DeviceKeyList[key.KeyIndex].Label = key.Name;
            foreach (Device device in DeviceList)
                device.UpdateDeviceKeys(key, updateFirmwareLabel);
        }

        public void RenumberKeySpecSequence()
        {
            int value = 1;
            foreach (DeviceKey key in DeviceKeyList)
            {
                if (key.Type == Key.KeyType.Standard)
                {
                    string nonDigits = Common.NonDigits(key.KeySpecNumber);
                    key.KeySpecNumber = value + nonDigits;
                    if (string.IsNullOrEmpty(nonDigits))
                        value++;
                }
            }
        }

        public void ResetDeviceKey(int keyNumber)
        {
            ResetDeviceKey(this, keyNumber);
        }

        public void ResetDeviceKey(Device dev, int keyNumber)
        {
            DeviceKey deviceKey = dev.GetDeviceKey(keyNumber);
            if (deviceKey == null) return;
            deviceKey.FirmwareLabel = string.Empty;
            deviceKey.IntronList.Clear();
            deviceKey.KeyIndex = keyNumber;
            if (dev.DeviceList.Count > 0)
                foreach (Device subdev in dev.DeviceList)
                    ResetDeviceKey(subdev, keyNumber);
        }

        public void RemoveDeviceKey(Key key)
        {
            RemoveDeviceKey(this, key);
        }

        private void RemoveDeviceKey(Device dev, Key key)
        {
            DeviceKey remove = dev.GetDeviceKey(key.KeyIndex);
            dev.DeviceKeyList.Remove(remove);
            foreach (DeviceKey key1 in dev.DeviceKeyList)
                if (key1.KeyIndex > key.KeyIndex)
                    key1.KeyIndex--;

            if (dev.DeviceList.Count > 0)
                foreach (Device subdev in dev.DeviceList)
                    RemoveDeviceKey(subdev, key);
        }

        public void DefaultBitmapGroups(bool setDefaultGroups)
        {
            foreach (DeviceKey key in DeviceKeyList)
            {
                if (!setDefaultGroups)
                {
                    key.BitmapGroup = Common.NoGroup;
                    continue;
                }
                key.BitmapGroup = Common.NoGroup;
                if (Common.DigitKeys.Contains(key.FirmwareLabel))
                    key.BitmapGroup = 0;
                if (Common.VolumeKeys.Contains(key.FirmwareLabel))
                    key.BitmapGroup = 1;
                if (Common.ChannelKeys.Contains(key.FirmwareLabel))
                    key.BitmapGroup = 2;
            }
        }

        public void AllowIntronDupes ( bool setFlag )
        {
            foreach ( DeviceKey key in DeviceKeyList )
            {
                key.AllowIntronDupes = setFlag;
            }

        }

        public virtual PickFunction.List GetFunctions(PickFunction.List functionList)
        {
            return functionList;
        }

        public PickId.List FilterAndSortForIDs(PickId.List pickIDs)
        {
            PickId.List filteredIDs = new PickId.List();
            foreach (char sourceDevice in SourceDevice)
            {
                foreach (PickId pickID in pickIDs)
                {
                    if (MatchesDevice(sourceDevice, pickID.Header.ID) == false)
                        continue;

                    if (DoesMatch(pickID))
                        filteredIDs.Add(pickID);
                }
            }

            return filteredIDs;
        }

        protected bool MatchesDevice(char device, string idName)
        {
            return char.IsLetter(idName[0]) && device == idName[0];
        }

        public abstract bool DoesMatch(PickId pickID);

        [Serializable]
        public class List : List<Device>
        {
            public override string ToString()
            {
                if (Count == 0)
                    return string.Empty;

                string returnString = string.Empty;
                foreach (Device device in this)
                    returnString += device + ", ";
                return returnString.Remove(returnString.Length - 2);
            }

            public PickId.List FilterAndSortIDsByDeviceLetter(PickId.List sourceIDList, bool includeChildren)
            {
                PickId.List idListForDevices = new PickId.List();

                List<char> deviceLetters = GetDeviceLetters(includeChildren);
                foreach (char deviceLetter in deviceLetters)
                {
                    foreach (PickId pickID in sourceIDList)
                    {
                        if (pickID.Header.ID[0] == deviceLetter)
                            idListForDevices.Add(pickID);
                    }
                }

                return idListForDevices;
            }

            private List<char> GetDeviceLetters(bool includeChildren)
            {
                List<char> deviceLetters = new List<char>();
                foreach (Device device in this)
                {
                    foreach (char sourceDevice in device.SourceDevice)
                    {
                        if (deviceLetters.Contains(sourceDevice) == false)
                            deviceLetters.Add(sourceDevice);
                    }

                    if (includeChildren)
                    {
                        List<char> childDeviceLetters = device.DeviceList.GetDeviceLetters(true);
                        foreach (char childDeviceLetter in childDeviceLetters)
                        {
                            if (deviceLetters.Contains(childDeviceLetter) == false)
                                deviceLetters.Add(childDeviceLetter);
                        }
                    }
                }
                return deviceLetters;
            }
        }
    }
}

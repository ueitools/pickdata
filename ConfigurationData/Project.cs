using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace PickData
{
    [Serializable]
    public class Project : IProject
    {        
        public string FileCompatibilityVersion = "1.00";
        public string Language = "English";
        private string _projectName = "Empty";
        private string _projectDescription = string.Empty;
        private string _author;
        private DateTime _utcCreationDate;
        private string _customerName;
        private string _chipName;
        private Mode.List _modeList = new Mode.List();
        private Device.List _deviceList = new Device.List();
        private Key.List _keyList = new Key.List();
        private List<string> _idList = new List<string>();

        #region Accessors

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("This will match the name in the project database")]
        public string ProjectName
        {
            set { _projectName = value; }
            get { return _projectName; }
        }

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("A short discription of the project supplied by the user")]
        public string ProjectDescription
        {
            set { _projectDescription = value; }
            get { return _projectDescription; }
        }

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("Your Name Here")]
        public string Author
        {
            set { _author = value; }
            get { return _author; }
        }

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("The UTC date and time this project is created")]
        public DateTime UtcCreationDate
        {
            set { _utcCreationDate = value; }
            get { return _utcCreationDate; }
        }

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("Customer or Client for this project")]
        public string CustomerName
        {
            set { _customerName = value; }
            get { return _customerName; }
        }

        [CategoryAttribute("Project Settings"),
        DescriptionAttribute("The Name of the Chip Manufacturer for this project")]
        public string ChipName
        {
            get { return _chipName; }
            set { _chipName = value; }
        }

        [Browsable(Common.debugOnly)]
        public Mode.List ModeList
        {
            set { _modeList = value; }
            get { return _modeList; }
        }

        [Browsable(Common.debugOnly)]
        public Device.List DeviceList
        {
            set { _deviceList = value; }
            get { return _deviceList; }
        }

        [Browsable(Common.debugOnly)]
        public Key.List KeyList
        {
            set { _keyList = value; }
            get { return _keyList; }
        }

        [Browsable(Common.debugOnly)]
        public List<string> IdList
        {
            set { _idList = value; }
            get { return _idList; }
        }
#endregion

        public Mode AddMode(string modeName, string defaultId)
        {
            if (FindMode(modeName) != null)
                throw new Exception("Mode \"" + modeName + "\" Already Exists");
            Mode mode = new Mode();
            mode.GetDeviceList = new GetModeDeviceList(ModeDeviceList);
            mode.Name = modeName;
            mode.DefaultId = defaultId;
            ModeList.Add(mode);
            return mode;
        }

        public Mode FindMode(string modeName)
        {
            foreach (Mode mode in ModeList)
            {
                if (mode.Name == modeName)
                    return mode;
            }
            return null;
        }

        public void AllowIntronDupes ( bool setFlag )
        {
            foreach ( Mode mode in ModeList )
            {

                mode.AllowIntronDupes ( setFlag );

            }
        }

        public void DefaultBitmapGroups(bool setDefaultGroups)
        {
            foreach (Mode mode in ModeList)
            {
                mode.BitmapGroups.Clear();
                if (setDefaultGroups)
                {
                    mode.BitmapGroups.Add(0);
                    mode.BitmapGroups.Add(1);
                    mode.BitmapGroups.Add(2);
                }
                mode.DefaultBitmapGroups(setDefaultGroups);
            }

        }

        public void AddDevice(Device dev, bool initKeyList)
        {
            Mode mode = FindMode(dev.Mode) ?? AddMode(dev.Mode, string.Empty);
            if (DeviceExistsInList(mode.DeviceList, dev))
                throw new Exception("Device Already Exists");

            foreach (Device device in DeviceList)
            {
                if (device.DeviceName == dev.DeviceName &&
                    device.Mode == dev.Mode)
                    dev = device;
            }
            if (!DeviceList.Contains(dev))
                DeviceList.Add(dev);
            dev.Mode = mode.Name;
            if (initKeyList)
                InitializeDeviceKeyList(dev);
        }

        public void InsertChildDevice(Device dev, Device inDevice, bool initializeKeyList)
        {
            if (DeviceExistsInList(inDevice.DeviceList, dev))
                throw new Exception("Device Already Exists");
            inDevice.DeviceList.Add(dev);
            if (initializeKeyList)
                InitializeDeviceKeyList(dev);
        }

        private void InitializeDeviceKeyList(Device dev)
        {
            dev.DeviceKeyList.Clear();
            foreach (Key key in KeyList)
                dev.DeviceKeyList.Add(new DeviceKey(key));
        }

        private void UpdateModes()
        {
            foreach (Mode mode in ModeList)
                mode.GetDeviceList = new GetModeDeviceList(ModeDeviceList);
        }

        private Device.List ModeDeviceList(Mode mode)
        {
            Device.List modeDevices = new Device.List();
            foreach (Device device in DeviceList)
            {
                if (device.Mode.Equals(mode.Name))
                    modeDevices.Add(device);
            }
            return modeDevices;
        }

        private bool DeviceExistsInList(IEnumerable<Device> list, Device dev)
        {
            foreach (Device device in list)
                if (device.ToString() == dev.ToString())
                    return true;
            return false;
        }

        public Device FindDevice(char device)
        {
            Device result = null;
            foreach (Mode mode in ModeList)
            {
                result = FindDevice(mode.DeviceList, device);
                if (result != null) break;
            }
            return result;
        }

        private Device FindDevice(Device.List list, char device)
        {
            foreach (Device dev in list)
            {
                if (dev.DeviceName == device)
                    return dev;
                if (dev.DeviceList.Count > 0)
                {
                    Device subdev = FindDevice(dev.DeviceList, device);
                    if (subdev != null)
                        return subdev;
                }
            }
            return null;
        }

        public void DeleteDevice(Device dev)
        {
            DeleteDevice(DeviceList, dev);
            for (int index = 0; index < ModeList.Count; index++)
            {
                while (index < ModeList.Count && ModeList[index].DeviceList.Count == 0)
                    ModeList.Remove(ModeList[index]);
            }
        }

        private void DeleteDevice(Device.List list, Device device)
        {
            list.Remove(device);
            foreach (Device dev in list)
            {
                if (dev.DeviceList.Count > 0)
                    DeleteDevice(dev.DeviceList, device);
            }
        }

        public Key AllocateKey(TemplateItem value, int keyNumber, Key.KeyType type)
        {
            Key key = new Key(value, keyNumber, type);
            return AllocateKey(key, keyNumber);
        }

        #region Code to handle shifting key isse
        public Key AllocateKey ( TemplateItem value, int keyNumber, Key.KeyType type,int _specno)
        {
            Key key = new Key ( value, keyNumber, type,_specno );
            return AllocateKey ( key, keyNumber );
        }
        private Key AllocateKey ( Key key, int keyNumber,int _specno )
        {
            foreach ( Key key1 in KeyList )
            {
                if ( key1.KeyIndex >= keyNumber )
                    key1.KeyIndex++;
            }
            
            KeyList.Add ( key );
            KeyList.Sort ( );

            foreach ( Device device in DeviceList )
                device.InsertDeviceKey ( key, keyNumber );

            return key;
        }

        #endregion

        private Key AllocateKey(Key key, int keyNumber)
        {
            foreach (Key key1 in KeyList)
            {
                if (key1.KeyIndex >= keyNumber)
                    key1.KeyIndex++;
            }
            KeyList.Add(key);
            KeyList.Sort();

            foreach (Device device in DeviceList)
                device.InsertDeviceKey(key, keyNumber);

            return key;
        }

        public void EqualizeKeyLists(Key key)
        {
            foreach (Mode mode in ModeList)
                mode.EqualizeKeyLists(key);
        }

        public void MoveKey(int sourceRow, int targetRow)
        {
            MoveDeviceKey(DeviceList, sourceRow, targetRow);

            int startRow = (sourceRow < targetRow) ? sourceRow : targetRow;
            int endRow = (sourceRow > targetRow) ? sourceRow : targetRow;
            for (int row = startRow; row <= endRow; row++)
            {
                KeyList[row].KeyIndex += ((sourceRow < targetRow) ? -1 : 1);
            }
            KeyList[sourceRow].KeyIndex = targetRow;
            KeyList.Sort();
        }

        private void MoveDeviceKey(Device.List deviceList, int sourceRow, int targetRow)
        {
            foreach (Device device in deviceList)
            {
                if (device.DeviceList.Count > 0)
                    MoveDeviceKey(device.DeviceList, sourceRow, targetRow);

                int startRow = (sourceRow < targetRow) ? sourceRow : targetRow;
                int endRow = (sourceRow > targetRow) ? sourceRow : targetRow;
                for (int row = startRow; row <= endRow; row++)
                {
                    device.DeviceKeyList[row].KeyIndex += ((sourceRow < targetRow) ? -1 : 1);
                }
                device.DeviceKeyList[sourceRow].KeyIndex = targetRow;
                device.DeviceKeyList.Sort();
            }
        }

        public void RenumberKeySpecSequence()
        {
            int value = 1;
            foreach (Key key in KeyList)
            {
                if (key.Type == Key.KeyType.Standard)
                {
                    string nonDigits = Common.NonDigits(key.KeySpecNumber);
                    key.KeySpecNumber = value + nonDigits;
                    if (string.IsNullOrEmpty(nonDigits))
                        value++;
                }
            }
            RenumberDeviceKeySpecSequence(DeviceList);
        }

        private void RenumberDeviceKeySpecSequence(Device.List deviceList)
        {
            foreach (Device device in deviceList)
            {
                if (device.DeviceList.Count > 0)
                    RenumberDeviceKeySpecSequence(device.DeviceList);
                device.RenumberKeySpecSequence();
            }
        }

        public void RemoveKey(Key key)
        {
            int keyIndex = key.KeyIndex;
            foreach (Mode mode in ModeList)
            {
                foreach (Device device in mode.DeviceList)
                    device.RemoveDeviceKey(key);
            }
            KeyList.Remove(key);
            foreach (Key key1 in KeyList)
            {
                if (key1.KeyIndex > keyIndex)
                    key1.KeyIndex--;
            }
        }

        public Key FindKey(string firmwareLabel, Key.KeyType type)
        {
            foreach (Key key1 in KeyList)
                if (key1.FirmwareLabel == firmwareLabel && key1.Type == type)
                    return key1;
            return null;
        }

        public Key FindKey(string firmwareLabel)
        {
            foreach (Key key in KeyList)
                if (key.FirmwareLabel == firmwareLabel)
                    return key;
            return null;
        }

        public DeviceKey FindDeviceKey(string firmwareLabel)
        {
            DeviceKey result = null;
            foreach (Mode mode in ModeList)
            {
                foreach (Device device in mode.DeviceList)
                {
                    result = device.FindDeviceKey(firmwareLabel);
                    if (result != null)
                        return result;
                }
            }
            return result;
        }

        /// <summary>
        /// Used to move the bitmap group keys to the top of the list in group order
        /// keys within groups will be in the order specified in the Common.cs key lists
        /// This will NOT change the order of other keys (non bitmap group keys) 
        /// if you want bitmap optimized order call OptimizeKeyList instead
        /// </summary>
        /// <param name="id">the ID this key list is for</param>
        /// <param name="keys">the PickResultKey key list</param>
        /// <returns>orderd set of PickResultKeys</returns>
        public List<PickResultKey> MoveBitmapGroupsToTheTop(string id, List<PickResultKey> keys)
        {
            Device dev = FindDevice(id[0]);
            if (dev == null)
                throw new Exception(string.Format("Unable to find the Device for ID {0}", id));
            return FindMode(dev.Mode).MoveBitmapGroupsToTheTop(dev, keys);
        }

        /// <summary>
        /// Used to first move bitmap group keys to the top then set the rest of the keys in 
        /// bitmap optimization order.  keys not listed in either optimizations 
        /// should stay in the same order at the end of the list
        /// There is NO NEED to call MoveBitmapGroupsToTheTop first, this id done internally
        /// </summary>
        /// <param name="id">the ID this key list is for</param>
        /// <param name="keys">the PickResultKey key list</param>
        /// <returns>orderd set of PickResultKeys</returns>
        public List<PickResultKey> OptimizeKeyList(string id, List<PickResultKey> keys)
        {
            Device dev = FindDevice(id[0]);
            if (dev == null)
                throw new Exception(string.Format("Unable to find the Device for ID {0}", id));
            return FindMode(dev.Mode).SetOptimizedKeyOrder(dev, keys);
        }

        public void Clear()
        {
            Language = "English";
            ProjectName = Common.EmptyProjectName;
            Author = string.Empty;
            CustomerName = string.Empty;
            UtcCreationDate = DateTime.UtcNow;
            ModeList.Clear();
            KeyList.Clear();
            IdList.Clear();
            DeviceList.Clear();
            //DeviceListHolder.Clear();
        }

        private static int _deviceOrderCount;
        private static void SetDeviceOrder(Device.List deviceList)
        {
            foreach (Device device in deviceList)
            {
                device.DeviceOrder = _deviceOrderCount++;
                if (device.DeviceList.Count > 0)
                    SetDeviceOrder(device.DeviceList);
            }
        }

        public MemoryStream ToXml()
        {
            _deviceOrderCount = 0;
            foreach (Mode mode in ModeList)
                SetDeviceOrder(mode.DeviceList);
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            XmlSerializer x = new XmlSerializer(GetType());
            x.Serialize(writer, this);
            return stream;
        }

        /// <summary>
        /// Load configuration data from a file
        /// </summary>
        /// <param name="fileName">full path & file name</param>
        /// <param name="prj">project structure</param>
        public void FromXml(string fileName, ref Project prj)
        {
            if (!File.Exists(fileName)) return;
            Stream tr = new FileStream(fileName, FileMode.Open);
            FromXml(tr, ref prj);
        }

        /// <summary>
        /// Load configuration data from an XML data stream
        /// </summary>
        /// <param name="streamIn">Stream</param>
        /// <param name="prj">Project structure</param>
        public void FromXml(Stream streamIn, ref Project prj)
        {
            Clear();
            XmlSerializer x = new XmlSerializer(prj.GetType());
            try
            {
                prj = (Project)x.Deserialize(streamIn);
                streamIn.Close();
                prj.UpdateModes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public interface IProject
    {
        string ProjectName { set; get; }
        string Author { set; get; }
        DateTime UtcCreationDate { set; get; }
        string CustomerName { set; get; }
        Mode.List ModeList { set; get; }
        Key.List KeyList { set; get; }
        List<string> IdList { set; get; }
        Device.List DeviceList { get; }

        void FromXml(string fileName, ref Project prj);
        List<PickResultKey> MoveBitmapGroupsToTheTop(string id, List<PickResultKey> keys);
        List<PickResultKey> OptimizeKeyList(string id, List<PickResultKey> keys);
        Device FindDevice(char deviceName);
    }
}
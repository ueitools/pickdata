
namespace PickData
{
    static class DeviceListHolder
    {
        private static Device.List _deviceList = new Device.List();

        public static Device.List DeviceList
        {
            get { return _deviceList; }
            set { _deviceList = value; }
        }

        public static void Clear()
        {
            DeviceList.Clear();
        }

        public static Device.List ModeDeviceList(Mode mode)
        {
            Device.List modeDevices = new Device.List();
            foreach (Device device in DeviceList)
            {
                if (device.ModeList.Contains(mode.Name))
                    modeDevices.Add(device);
            }
            return modeDevices;
        }

    }
}

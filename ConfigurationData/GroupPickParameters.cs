using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class GroupPickParameters
    {
        private int _groupNumber;
        private bool _bestPick;
        private int _firstColumnCount;
        private int _firstColumnPercent;
        private int _remainingColumnPercent;

        public GroupPickParameters()
        {
        }

        public GroupPickParameters(GroupPickParameters sourceParameters)
        {
            _groupNumber = sourceParameters.GroupNumber;
            _firstColumnCount = sourceParameters.FirstColumnCount;
            _firstColumnPercent = sourceParameters.FirstColumnPercent;
            _remainingColumnPercent = sourceParameters.RemainingColumnPercent;
            _bestPick = sourceParameters.BestPick;
        }

        public GroupPickParameters(int groupNumber, int firstColumnCount, int firstColumnPercent, int columnPercent, bool bestPick)
        {
            _groupNumber = groupNumber;
            _firstColumnCount = firstColumnCount;
            _firstColumnPercent = firstColumnPercent;
            _remainingColumnPercent = columnPercent;
            _bestPick = bestPick;
        }

        public int GroupNumber
        {
            set { _groupNumber = value; }
            get { return _groupNumber; }
        }

        public int FirstColumnCount
        {
            set { _firstColumnCount = value; }
            get { return _firstColumnCount; }
        }

        public int FirstColumnPercent
        {
            set { _firstColumnPercent = value; }
            get { return _firstColumnPercent; }
        }

        public int RemainingColumnPercent
        {
            set { _remainingColumnPercent = value; }
            get { return _remainingColumnPercent; }
        }

        public bool BestPick
        {
            set { _bestPick = value; }
            get { return _bestPick; }
        }

        public bool IsCheckPair
        {
            get
            {
                return (_firstColumnCount == 1 && _firstColumnPercent == 100 && 
                        _remainingColumnPercent == 100 && !_bestPick);
            }
        }

        [Serializable]
        public class List : List<GroupPickParameters> { }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;

namespace PickData
{
    public class TemplateItem  : IComparable<TemplateItem>
    {
        public string Source = string.Empty;        // used for drag & drop
        private string _device = "Unclassified";
        private string _keyName = string.Empty;
        private int _sortingOrder;
        private string _popular;
        private string _firmwareLabel = string.Empty;
        private string _intron = string.Empty;
        private int _pickPriority;
        private string _data = string.Empty;
        private string _customerData = string.Empty;
        private string _synth = string.Empty;

#region Accessors

        public string Synth
        {
            get { return _synth; }
            set { _synth = value.Trim(); }
        }

        public string Device
        {
            get { return _device;}
            set { _device = value.Trim(); }
        }
        public string KeyName
        {
            get { return _keyName;}
            set { _keyName = value.Trim(); }
        }
        public int SortingOrder
        {
            get { return _sortingOrder; }
            set { _sortingOrder = value; }
        }
        public string Popular
        {
            get { return _popular; }
            set { _popular = value.Trim(); }
        }
        public string FirmwareLabel
        {
            get { return _firmwareLabel; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;
                _firmwareLabel = value.ToLower().Trim();
            }
        }
        public string Intron
        {
            get { return _intron; }
            set { _intron = value.Trim(); }
        }
        public int PickPriority
        {
            get { return _pickPriority; }
            set { _pickPriority = value; }
        }
        public string Data
        {
            get { return _data; }
            set { _data = value.Trim(); }
        }
        public string CustomerData
        {
            get { return _customerData; }
            set { _customerData = value.Trim(); }
        }
#endregion

        public TemplateItem(){}
        public TemplateItem(IntronData intron)
        {
            //intron.IntronType;
            _intron = intron.Intron;
            _keyName = intron.Label;
            _data = intron.Data;
            //intron.Link;
            _customerData = intron.CustomerData;
        }

        public Hashtable TemplateHash
        {
            set
            {
                _device = value["Device"].ToString();
                _keyName = value["Key_Name"].ToString();

                if (value["Sorting_Order"] != null)
                    _sortingOrder = (int)value["Sorting_Order"];
                if (value["Popular"] != null)
                    _popular = value["Popular"].ToString();
                if (value["Firmware_Label"] != null)
                    _firmwareLabel = value["Firmware_Label"].ToString();
                if (value["Intron"] != null)
                    _intron = value["Intron"].ToString();
                if (value["Pick_Priority"] != null)
                    _pickPriority = (int)value["Pick_Priority"];
            }
        }

        public Hashtable IntronHash
        {
            set
            {
                if (value["Category"] != null)
                    _device = value["Category"].ToString();
                if (value["Label"] != null)
                    _keyName = value["Label"].ToString();
                if (value["Intron"] != null)
                    _intron = value["Intron"].ToString();
            }
        }

        public static int TemplateByPickPriority(TemplateItem item1, TemplateItem item2)
        {
            if (item1.PickPriority < item2.PickPriority) return -1;
            if (item1.PickPriority > item2.PickPriority) return 1;
            return 0;
        }

        int IComparable<TemplateItem>.CompareTo(TemplateItem other)
        {
            if (_sortingOrder < other._sortingOrder) return -1;
            if (_sortingOrder > other._sortingOrder) return 1;
            return 0;
        }

        public class List : List<TemplateItem> { }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PickData
{
    public delegate Device.List GetModeDeviceList(Mode mode);

    [Serializable]
    public class Mode
    {
        private string _defaultId;
        private string _name;
        private List<int> _bitmapGroups = new List<int>();
        private List<OptomizedKeyInfo> _optomizedKeyOrder = new List<OptomizedKeyInfo>();

        [XmlIgnore]
        public GetModeDeviceList GetDeviceList;

#region Accessors

        [CategoryAttribute("Mode Settings"),
        DescriptionAttribute("A list of existing Bitmap Groups")]
        public List<int> BitmapGroups
        {
            get { return _bitmapGroups; }
            set { _bitmapGroups = value; }
        }

        [CategoryAttribute("Mode Settings"),
        DescriptionAttribute("The name of this mode")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (DeviceList != null && DeviceList.Count > 0)
                {
                    foreach (Device device in DeviceList)
                        device.Mode = value;
                }
                _name = value;
            }
        }

        [CategoryAttribute("Mode Settings"),
        DescriptionAttribute("The ID this mode will use on first power up of the device")]
        public string DefaultId
        {
            get { return _defaultId; }
            set { _defaultId = value; }
        }

        [CategoryAttribute("Mode Settings"),
        DescriptionAttribute("A list of existing Bitmap Groups"),
        ReadOnly(true)]
        [XmlIgnore]
        public Device.List DeviceList
        {
            get { return (GetDeviceList == null)?null:GetDeviceList(this); }
        }

        [Browsable(Common.debugOnly)]
        public List<OptomizedKeyInfo> OptomizedKeyOrder
        {
            get { return _optomizedKeyOrder; }
            set { _optomizedKeyOrder = value; }
        }

#endregion Accessors

        public void EqualizeKeyLists(Key key)
        {
            bool found = false;
            foreach (Device device in DeviceList)
            {
                if (device.FindDeviceKey(key.FirmwareLabel) != null)
                {
                    found = true;
                    break;
                }
            }
            if (!found) return;
            foreach (Device device in DeviceList)
            {
                device.DeviceKeyList[key.KeyIndex].FirmwareLabel = key.FirmwareLabel;
            }
        }

        public void SetDeviceKeyToBitmapGroup(string firmwareLabel, int bitmapGroup)
        {
            if (bitmapGroup > Common.NoGroup && FindBitmapGroup(bitmapGroup) == Common.NoGroup)
                throw new Exception("Tried to set a key to a Bitmap Group that doesn't exist");

            SetDeviceKeyToBitmapGroupEntireMode(DeviceList, firmwareLabel, bitmapGroup);
        }

        private void SetDeviceKeyToBitmapGroupEntireMode(Device.List deviceList, string firmwareLabel, int bitmapGroup)
        {
            foreach (Device device in deviceList)
            {
                if (device.DeviceList.Count > 0)
                    SetDeviceKeyToBitmapGroupEntireMode(device.DeviceList, firmwareLabel, bitmapGroup);
                DeviceKey key = device.FindDeviceKey(firmwareLabel);
                if (key != null)
                    key.BitmapGroup = bitmapGroup;
            }
        }

        public void RemoveBitmapGroupIfEmpty(int bitmapGroup)
        {
            int keycount = 0;
            CountAllBitmapGroupEntries(DeviceList, bitmapGroup, ref keycount);
            if (keycount == 0)
                BitmapGroups.Remove(FindBitmapGroup(bitmapGroup));
        }

        private void CountAllBitmapGroupEntries(Device.List deviceList, int bitmapGroup, ref int keycount)
        {
            foreach (Device device in deviceList)
            {
                if (device.DeviceList.Count > 0)
                    CountAllBitmapGroupEntries(device.DeviceList, bitmapGroup, ref keycount);
                foreach (DeviceKey key in device.DeviceKeyList)
                {
                    if (key.BitmapGroup == bitmapGroup) keycount++;
                }
            }
        }

        private int FindBitmapGroup(int num)
        {
            foreach (int group in BitmapGroups)
            {
                if (group == num)
                    return group;
            }
            return Common.NoGroup;
        }

        public void AllowIntronDupes ( bool setFlag )
        {
            AllowIntronDupes ( DeviceList, setFlag );
        }

        private void AllowIntronDupes ( Device.List deviceList, bool setDefaultGroups )
        {
            foreach ( Device device in deviceList )
            {
                if ( device.DeviceList.Count > 0 )
                    AllowIntronDupes ( device.DeviceList, setDefaultGroups );
                device.AllowIntronDupes ( setDefaultGroups );
            }
        }

        public void DefaultBitmapGroups(bool setDefaultGroups)
        {
            DefaultBitmapGroups(DeviceList, setDefaultGroups);
        }

        private void DefaultBitmapGroups(Device.List deviceList, bool setDefaultGroups)
        {
            foreach (Device device in deviceList)
            {
                if (device.DeviceList.Count > 0)
                    DefaultBitmapGroups(device.DeviceList, setDefaultGroups);
                device.DefaultBitmapGroups(setDefaultGroups);
            }
        }

        public int NextBitmapGroupNumber()
        {
            int groupNumber = 0;
            if (BitmapGroups.Count > 0)
            {
                foreach (int group in BitmapGroups)
                {
                    if (group >= groupNumber)
                        groupNumber = group+1;
                }
            }
            BitmapGroups.Add(groupNumber);
            return groupNumber;
        }

        public List<PickResultKey> SetOptimizedKeyOrder(Device dev, List<PickResultKey> keys)
        {
            keys = MoveBitmapGroupsToTheTop(dev, keys);

            List<PickResultKey> result = new List<PickResultKey>();
            foreach (PickResultKey key in keys)
            {
                if(!string.IsNullOrEmpty(key.FirmwareLabel))
                {
                DeviceKey deviceKey = dev.FindDeviceKey(key.FirmwareLabel);
                if (deviceKey.BitmapGroup >= 0)
                    result.Add(key);
                }
            }
            foreach (OptomizedKeyInfo keyInfo in OptomizedKeyOrder)
            {
                foreach (PickResultKey key in keys)
                    if (!result.Contains(key) && keyInfo.FirmwareLabel.Equals(key.FirmwareLabel))
                        result.Add(key);
            }
            foreach (PickResultKey key in keys)
            {
                if (!result.Contains(key))
                    result.Add(key);
            }
            return result;
        }

        public List<PickResultKey> MoveBitmapGroupsToTheTop(Device dev, List<PickResultKey> keys)
        {
            sortDevice = dev;
            List<PickResultKey> result = new List<PickResultKey>();
            foreach (PickResultKey key in keys)
            {
                //This condition added only for 'Test ID'
                //In this case all the keys were considered while run pick
                //Hence for keys with no firmware label it was throwing an exception
                //Need to test this since this should not impact the normal flow
                if ( !string.IsNullOrEmpty ( key.FirmwareLabel ) )
                {
                    DeviceKey deviceKey = dev.FindDeviceKey ( key.FirmwareLabel );
                    if ( deviceKey.BitmapGroup >= 0 )
                        result.Add ( key );
                }
            }
            result.Sort(SortBitmapGroupsToTheTop);
            foreach (PickResultKey key in keys)
                if (!result.Contains(key))
                    result.Add(key);
            return result;
        }

        private Device sortDevice;      // used only for the sort below

        private int SortBitmapGroupsToTheTop(PickResultKey f1, PickResultKey f2)
        {
            DeviceKey deviceKey1 = sortDevice.FindDeviceKey(f1.FirmwareLabel);
            DeviceKey deviceKey2 = sortDevice.FindDeviceKey(f2.FirmwareLabel);
            int bmGroup1 = (deviceKey1 == null) ? -1 : deviceKey1.BitmapGroup;
            int bmGroup2 = (deviceKey2 == null) ? -1 : deviceKey2.BitmapGroup;
            if (bmGroup1 > -1 || bmGroup2 > -1)
            {
                if (bmGroup1 == -1) return 1;
                if (bmGroup2 == -1) return -1;
                if (bmGroup1 == bmGroup2)
                    return SpecifiedBmGroupOrder(f1, f2);
                return bmGroup1.CompareTo(bmGroup2);
            }
            return 0;
        }

        private int SpecifiedBmGroupOrder(PickResultKey k1, PickResultKey k2)
        {
            string f1 = k1.FirmwareLabel;
            string f2 = k2.FirmwareLabel;
            if (Common.DigitKeys.Contains(f1) && Common.DigitKeys.Contains(f2))
                return Common.DigitKeys.IndexOf(f1).CompareTo(Common.DigitKeys.IndexOf(f2));
            if (Common.VolumeKeys.Contains(f1) && Common.VolumeKeys.Contains(f2))
                return Common.VolumeKeys.IndexOf(f1).CompareTo(Common.VolumeKeys.IndexOf(f2));
            if (Common.ChannelKeys.Contains(f1) && Common.ChannelKeys.Contains(f2))
                return Common.ChannelKeys.IndexOf(f1).CompareTo(Common.ChannelKeys.IndexOf(f2));
            return f1.CompareTo(f2);
        }

        [Serializable]
        public class List : List<Mode> { }
    }
    [Serializable]
    public class OptomizedKeyInfo
    {
        private string _firmwareLabel;
        private int _optomizedOrder;

        public string FirmwareLabel
        {
            get { return _firmwareLabel; }
            set { _firmwareLabel = value.ToLower(); }
        }

        public int OptomizedOrder
        {
            get { return _optomizedOrder; }
            set { _optomizedOrder = value; }
        }
    }
}

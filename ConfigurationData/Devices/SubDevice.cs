using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class SubDevice : Device, ITestSubDevice
    {
        private string _subDeviceSource;
        private List<char> _sourceDevice = new List<char>();

        private SubDevice() { }

        public SubDevice(string subDeviceSource, string mode, Device sourceDevice, char device, bool move)
        {
            _device = device;
            _modeSource = mode;
            _move = move;
            _subDeviceSource = subDeviceSource;
            _sourceDevice.Add(sourceDevice.DeviceName);
            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        public string SubDeviceSource
        {
            set { _subDeviceSource = value; }
            get { return _subDeviceSource; }
        }

        public override List<char> SourceDevice
        {
            get { return _sourceDevice; }
            set { _sourceDevice = value; }
        }

        public override bool RequiresExactMatch
        {
            get { throw new NotImplementedException(); }
        }

        public override int SortingWeight
        {
            get { return 40; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        private bool MatchesAnyDevice(PickId pickID)
        {
            foreach (char sourceDevice in _sourceDevice)
            {
                if (MatchesDevice(sourceDevice, pickID.Header.ID))
                    return true;
            }
            return false;
        }

        public override bool DoesMatch(PickId pickID)
        {
            
            if (MatchesAnyDevice(pickID) == false)
                return false;

            foreach (PickDevice pickDevice in pickID.DeviceList)
            {
                if (pickDevice.DeviceTypeList == null)
                    continue;

                foreach (PickDeviceType pickDeviceType in pickDevice.DeviceTypeList)
                    if (pickDeviceType.Name == _subDeviceSource)
                        return true;
            }

            return false;
        }

        public override string ToString()
        {
            if (_sourceDevice.Count == 1 && _sourceDevice[0] == _device)
                return _subDeviceSource;

            string deviceList = string.Empty;
            foreach (char dev in _sourceDevice)
                deviceList += dev;

            return string.Format("{0}[{1}]({2})", _device, deviceList, _subDeviceSource);
        }

        string ITestSubDevice.SubDeviceSource
        {
            get { return _subDeviceSource; }
            set { _subDeviceSource = value; }
        }
    }

    public interface ITestSubDevice
    {
        string SubDeviceSource { get; set; }
    }
}

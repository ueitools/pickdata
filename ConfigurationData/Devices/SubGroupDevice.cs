using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PickData
{
    [Serializable]
    public class SubGroupDevice : Device
    {
        private string _deviceName;
        private List<IdRenumber> _sourceIdList = new List<IdRenumber>();

        private SubGroupDevice() { }

        public SubGroupDevice(string name, Dictionary<string, string> source, string mode, 
                            Device sourceDevice, char device, bool move)
        {
            _deviceName = name;
            _device = device;
            _modeSource = mode;
            _move = move;

            foreach (KeyValuePair<string, string> item in source)
                _sourceIdList.Add(new IdRenumber(item));

            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        [CategoryAttribute("Sub-Group Device Settings"),
        Description("A User specified name for this Device")]
        public string CustomName
        {
            get { return _deviceName; }
            set { _deviceName = value; }
        }

        [CategoryAttribute("Sub-Group Device Settings"),
        Description("A list of one or more IDs that were specified when creating this Device")]
        public List<IdRenumber> SourceIdList
        {
            get { return _sourceIdList; }
            set { _sourceIdList = value; }
        }

        public int FindSourceID(string id)
        {
            foreach (IdRenumber idPair in _sourceIdList)
            {
                if (idPair.FromId.ToUpper().Equals(id.ToUpper()))
                    return _sourceIdList.IndexOf(idPair);
            }
            return - 1;
        }

        public string FindToId(string id)
        {
            foreach (IdRenumber idPair in _sourceIdList)
            {
                if (idPair.FromId.ToUpper().Equals(id.ToUpper()))
                    return idPair.ToId;
            }
            return string.Empty;
        }

        public override bool RequiresExactMatch
        {
            get { return true; }
        }

        public override int SortingWeight
        {
            get { return 20; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        public override bool DoesMatch(PickId pickID)
        {
            if (FindSourceID(pickID.Header.ID) >= 0)
                return true;

            return false;
        }

        public override List<char> SourceDevice
        {
            get
            {
                List<char> deviceList = new List<char>();
                foreach (IdRenumber id in _sourceIdList)
                {
                    if (!deviceList.Contains(id.FromId[0]))
                        deviceList.Add(id.FromId[0]);
                }
                return deviceList;
            }

            set { }
        }

        public override string ToString()
        {
            return _deviceName;
        }
    }

    public class IdRenumber
    {
        private string _fromId;
        private string _toId;

        public string FromId
        {
            get { return _fromId; }
            set { _fromId = value; }
        }

        public string ToId
        {
            get { return _toId; }
            set { _toId = value; }
        }

        public IdRenumber(){}
        public IdRenumber(KeyValuePair<string, string> pair)
        {
            _toId = pair.Key;
            _fromId = pair.Value;
        }
    }
}

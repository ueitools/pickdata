using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class MarketDevice : Device, ITestMarketDevice
    {
        private string _region;
        private List<char> _sourceDevice = new List<char>();

        private MarketDevice() { }

        public MarketDevice(string region, string mode, Device sourceDevice, char device, bool move)
        {
            _device = device;
            _modeSource = mode;
            _move = move;
            _region = region;
            _sourceDevice.Add(sourceDevice.DeviceName);
            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        public string Region
        {
            get { return _region; }
            set { _region = value;}
        }

        public override List<char> SourceDevice
        {
            get { return _sourceDevice; }
            set { _sourceDevice = value; }
        }

        public override bool RequiresExactMatch
        {
            get { return true; }
        }

        public override int SortingWeight
        {
            get { return 30; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        private bool MatchesAnyDevice(PickId pickID)
        {
            foreach (char sourceDevice in _sourceDevice)
            {
                if (MatchesDevice(sourceDevice, pickID.Header.ID))
                    return true;
            }
            return false;
        }

        public override bool DoesMatch(PickId pickID)
        {
            if (MatchesAnyDevice(pickID) == false)
                return false;

            foreach (PickDevice pickDevice in pickID.DeviceList)
            {
                if (pickDevice.CountryList == null)
                    continue;
             
                foreach (PickCountry pickCountry in pickDevice.CountryList)
                    if (pickCountry.Region == _region)
                        return true;
            }

            return false;
        }

        public override string ToString()
        {
            if (_sourceDevice.Count == 1 && _sourceDevice[0] == _device)
                return _region;

            string deviceList = string.Empty;
            foreach (char dev in _sourceDevice)
                deviceList += dev;
            return string.Format("{0}[{1}]({2})", _device, deviceList, _region);
        }

        string ITestMarketDevice.Region
        {
            get { return _region; }
            set { _region = value; }
        }
    }

    public interface ITestMarketDevice
    {
        string Region { get; set; }
    }
}

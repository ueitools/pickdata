using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class StandardDevice : Device
    {
        private StandardDevice() { }

        public StandardDevice(char device, string mode)
        {
            _device = device;
            _modeSource = mode;
            _move = false;
        }

        public override List<char> SourceDevice
        {
            get 
            {
                List<char> devices = new List<char>();
                devices.Add(_device);
                return devices;
            }
            set { }
        }

        public override bool RequiresExactMatch
        {
            get { return false; }
        }

        public override int SortingWeight
        {
            get { return 10; }
        }

        public override bool IsCustom
        {
            get { return false; }
        }

        public override bool DoesMatch(PickId pickID) {
            string id = pickID.Header.ID;
           
            return (char.IsLetter(id[0]) && _device == id[0]);
        }

        public override string ToString()
        {
            return _device.ToString();
        }
    }
}

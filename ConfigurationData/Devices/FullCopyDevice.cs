using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class FullCopyDevice : Device
    {
        private List<char> _sourceDevice = new List<char>();

        private FullCopyDevice() { }

        public FullCopyDevice(string mode, Device sourceDevice, char device, bool move)
        {
            _device = device;
            _modeSource = mode;
            _sourceDevice.Add(sourceDevice.DeviceName);
            _move = move;
            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        public override List<char> SourceDevice
        {
            get { return _sourceDevice; }
            set { _sourceDevice = value; }
        }

        public override bool RequiresExactMatch
        {
            get { return true; }
        }

        public override int SortingWeight
        {
            get { return 30; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        private bool MatchesAnyDevice(PickId pickID)
        {
            char device = pickID.Header.ID[0];
            return _sourceDevice.Contains(device);
        }

        public override bool DoesMatch(PickId pickID)
        {
            char device = pickID.Header.ID[0];
            return _sourceDevice.Contains(device);
        }

        public override string ToString()
        {
            string deviceList = string.Empty;
            foreach (char dev in _sourceDevice)
                deviceList += dev;
            return string.Format("{0}[{1}]", _device, deviceList);
        }
    }
}

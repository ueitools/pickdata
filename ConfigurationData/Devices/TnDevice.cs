using System;
using System.Collections.Generic;

namespace PickData
{
    [Serializable]
    public class TnDevice : Device
    {
        private String _idSource;
        private int _tnSource;
        private int _idRenumber = -1;

        private TnDevice() { }

        public TnDevice(string id, int renumber, int tnSource, string mode, Device sourceDevice, char device, bool move)
        {
            _idRenumber = renumber;
            _device = device;
            _modeSource = mode;
            _move = move;
            _tnSource = tnSource;
            _idSource = id;
            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        public override List<char> SourceDevice
        {
            get
            {
                List<char> devices = new List<char>();
                if (!string.IsNullOrEmpty(_idSource))
                    devices.Add(_idSource[0]);
                return devices;
            }
            set { }
        }

        public string IdSource
        {
            get { return _idSource; }
            set { _idSource = value; }
        }

        public int IdRenumber
        {
            get { return _idRenumber; }
            set { _idRenumber = value; }
        }

        public int TnSourceSource
        {
            get { return _tnSource; }
            set { _tnSource = value; }
        }

        public override bool RequiresExactMatch
        {
            get { return true; }
        }

        public override int SortingWeight
        {
            get { return 50; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        public override bool DoesMatch(PickId pickID)
        {
            if (pickID.Header.ID != _idSource)
                return false;

            foreach (PickDevice pickDevice in pickID.DeviceList)
                if (pickDevice.TN == _tnSource)
                    return true;

            return false;
        }

        public override string ToString()
        {
            string idName = _idSource;
            if (IdRenumber >= 0)
                idName = string.Format("{0}{1:D4}<{2}>[TN{3:D5}]", _device, IdRenumber, _idSource, _tnSource);
            else
                idName = string.Format("{0}{1}[TN{2:D5}]", _device, _idSource, _tnSource);

            if (_device == _idSource[0])
                return idName;
            return string.Format("{0}[{1}]{2}", _device, IdSource[0], idName);
        }
    }
}

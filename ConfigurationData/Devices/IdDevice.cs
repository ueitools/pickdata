using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace PickData
{
    [Serializable]
    public class IdDevice : Device
    {
        private List<string> _sourceIdList = new List<string>();
        private int _idRenumber = -1;

        private IdDevice() { }

        public IdDevice(List<string> source, int renumber, string mode, Device sourceDevice, char device, bool move)
        {
            if (source.Count == 1)
                _idRenumber = renumber;
            _device = device;
            _modeSource = mode;
            _move = move;
            _sourceIdList = source;
            foreach (DeviceKey item in sourceDevice.DeviceKeyList)
            {
                DeviceKeyList.Add(new DeviceKey(item));
            }
        }

        [CategoryAttribute("ID Device Settings"),
        Description("A list of one or more IDs that were specified when creating this Device")]
        public List<string> SourceIdList
        {
            get { return _sourceIdList; }
            set { _sourceIdList = value; }
        }

        public int IdRenumber
        {
            get { return _idRenumber; }
            set { if (SourceIdList.Count == 1) _idRenumber = value; }
        }

        public override bool RequiresExactMatch
        {
            get { return true; }
        }

        public override int SortingWeight
        {
            get { return 20; }
        }

        public override bool IsCustom
        {
            get { return SourceDevice.Count > 1 || SourceDevice[0] != _device; }
        }

        public override bool DoesMatch(PickId pickID)
        {
            if (_sourceIdList.Contains(pickID.Header.ID.ToUpper()))
                return true;

            return false;
        }

        public override List<char> SourceDevice
        {
            get
            {
                List<char> deviceList = new List<char>();
                foreach (string id in _sourceIdList)
                {
                    if (!deviceList.Contains(id[0]))
                        deviceList.Add(id[0]);
                }
                return deviceList;
            }

            set { }
        }

        public override string ToString()
        {
            string deviceList = string.Empty;
            foreach (string id in _sourceIdList)
                if (!deviceList.Contains(id[0].ToString()))
                    deviceList += id[0];

            string multiple = "";
            if (_sourceIdList.Count > 1)
                multiple = "...";

            string idName = SourceIdList[0];
            if (SourceIdList.Count == 1 && IdRenumber >= 0)
                idName = string.Format("{0}{1:D4}<{2}>", _device, IdRenumber, SourceIdList[0]);

            if (deviceList.Length == 1 && deviceList[0] == _device)
                return string.Format("{0}{1}", idName, multiple);

            return string.Format("{0}[{1}]{2}{3}", _device, deviceList, idName, multiple);

            //if (_sourceIdList.Count == 1 && _device != _sourceIdList[0][0])
            //    return string.Format("{0}[{1}]{2}", _device, _sourceIdList[0], multiple);

            //return string.Format("{0}[{1}]", _device, deviceList);
        }
    }
}

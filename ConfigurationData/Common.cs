using System.Collections.Generic;

namespace PickData
{
    public static class Common
    {
#if DEBUG
        public const bool debugOnly = true;
#else
        public const bool debugOnly = false;
#endif

        public const string EmptyProjectName = "Empty";
        public const int NoGroup = -1;
        public const string AllCategory = "All";

        public static readonly List<string> DigitKeys = new List<string>
            (new string[]{"k_dig0","k_dig1","k_dig2","k_dig3","k_dig4",
                          "k_dig5","k_dig6","k_dig7","k_dig8","k_dig9"});
        public static readonly List<string> VolumeKeys = new List<string>
            (new string[] {"k_volup", "k_voldn", "k_mute"});
        public static readonly List<string> ChannelKeys = new List<string>
            (new string[] { "k_chup", "k_chdn"});

        private static readonly string[] IntronPlus = { "+", ">" };
        private static readonly string[] IntronMinus = { "-", "<" };
        private static readonly string[] LabelPlus = {"UP", "RIGHT", "FFD", "FWD", "SKIPFWD", "FORWARD", "RHT"};
        private static readonly string[] LabelMinus = {"DN", "DOWN", "LEFT", "REWIND", "SKIPREV", "SLOWREV", "LFT"};

        public static bool IntronAssignmentError(string fwLabel, IntronData intron)
        {
            if (IntronContains(intron, IntronPlus) && LabelContains(fwLabel, LabelMinus)) 
                return true;
            if (IntronContains(intron, IntronMinus) && LabelContains(fwLabel, LabelPlus))
                return true;
            return false;
        }

        private static bool IntronContains(IntronData intron, IEnumerable<string> keywordList)
        {
            if (!string.IsNullOrEmpty(intron.Intron))
                foreach (string keyword in keywordList)
                {
                    if (intron.Intron.ToUpper().Contains(keyword))
                        return true;
                }
            return false;
        }

        private static bool LabelContains(string fwLabel, string[] keywordList)
        {
            foreach (string keyword in keywordList)
            {
                if (fwLabel.ToUpper().Contains(keyword))
                    return true;
            }
            return false;
        }

        public static string NonDigits(string value)
        {
            string result = string.Empty;
            foreach (char c in value)
            {
                if (!char.IsDigit(c))
                    result += c;
            }
            return result;
        }
    }
}
